//Hotel

db.users.insertOne({
	name: "single",
	accommodates: "2",
	price: 1000,
	description: "A simple room with all the basic necessities",
	roomsAvailable: "10",	
	isAvailable: false
});

db.users.insertMany([
	{
		name: "double",
		accommodates: "3",
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		roomsAvailable: "5",
		isAvailable: false
	},
	{
		name: "queen",
		accommodates: "4",
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple gateaway",
		roomsAvailable: "15",
		isAvailable: false
	}
]);

db.users.find({name: "double"});

db.users.updateOne({name: "queen"},
	{
		$set: {
			roomsAvailable: "0"
		}
	}
);

db.users.deleteMany({roomsAvailable: "0"});
